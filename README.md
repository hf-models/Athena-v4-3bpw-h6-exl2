---
license: cc-by-nc-4.0
---
quant of [IkariDev's](https://huggingface.co/IkariDev) [Athena-v4](https://huggingface.co/IkariDev/Athena-v4)

```
python3 convert.py \
    -i /input/IkariDev_Athena-v4/ \
    -o /output/temp/ \
    -c /input/wikitext/0000.parquet \
    -cf /output/3bpw/ \
    -b 3.0 \
    -hb 6
```